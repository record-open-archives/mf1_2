---
title: "MF1_2 simulator description"
---

<!-- Equation (using $$..$$) automatic numbering, use the tag \nonumber to deactivate for an individual equation -->
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  TeX: { 
      equationNumbers: { 
            autoNumber: "all",
            formatNumber: function (n) {return 'Eq.'+n}
      } 
  }
});
</script>

# MF1_2 : internal structure of the FarmType model

## Water Flows And Compartments
![MF1_2  Water Flows And Compartments](figures/Water_Flows_And_Compartments/WaterFlowsAndCompartments.svg)


## Water Balances equations

>Generic mass balance equation :  
$$
\Delta V = Inputs - Outputs
\label{eq:Mass_Balance}$$


### MF1_2 whole system

> $$V = Plots.SoilWaterContent + Pond.Volume + GroundWater.Volume + FarmIrrigationManager.WaterCycle\_RemainingAmount + Plots.RunOff$$  

> $$Inputs = Plot.Rain + Pond.Rain$$  

> $$Outputs = Plot.ETP + Plot.IrrigationLoss + Pond.Evap + Pond.Overflow + GroundWater.Discharge + GroundWater.GW\_Overland\_Flow$$  


### Plots sub-system

> $$V = Plot.SoilWaterContent$$  

> $$Inputs = Plot.Rain + Plot.Irrigation$$  

> $$Outputs = Plot.ETP + Plot.RunOff + Plot.Drain + Plot.IrrigationLoss$$  


### Pond sub-system

> $$V = Pond.Volume$$  

> $$Inputs = Plot.RunOff + Pond.Rain + Pond.Refill$$  

> $$Outputs = Pond.Evap + Pond.Percolation + Pond.Overflow + Pond.Extraction$$  


### GroundWater sub-system

> $$V = GroundWater.Volume$$  

> $$Inputs = Plots.Drain + Pond.Percolation$$  

> $$Outputs = Pump.Extraction + GroundWater.Discharge + GW\_Overland\_Flow$$  

### FarmIrrigationManager sub-system

> $$V = - FarmIrrigationManager.WaterCycle\_RemainingAmount$$  

> $$Inputs = Pond.Extraction + Pump.Extraction$$  

> $$Outputs = Plots.Irrigation + Pond.Refill$$  


## Top level
![MF1_2 top level](figures/MF1_2_top.svg)
![MF1_2 top level (Water only)](figures/Water_Flows_And_Compartments/MF1_2_top_Water.svg)

***

## Farm level
![MF1_2 Farm level](figures/MF1_2_Farm.svg)
![MF1_2 Farm level (Water only)](figures/Water_Flows_And_Compartments/MF1_2_Farm_Water.svg)

***

## Plot level
![MF1_2 Plot level](figures/MF1_2_Plot.svg)
![MF1_2 Plot level (Water only)](figures/Water_Flows_And_Compartments/MF1_2_Plot_Water.svg)

