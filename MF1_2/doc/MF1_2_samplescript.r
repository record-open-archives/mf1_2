###Options modifiables###
DO_EXPORT <- TRUE

DEBUT_SIMULATION <- '2014-1-1';
DUREE_SIMULATION <- 366;

FICHIER_CLIMAT <- 'Data_flux_tower.txt';

Irrig_Rule_Strat <- "None" #{None, Rain_Threshold, SoilWaterContent_Threshold, CropWaterStress_Threshold}

fw <- 1.0;
fw_Rain_Threshold <- 5.0;
REW_AWC_ratio <- 2/3;

Failure_Option <- 0; #{0:none, 1:water, 2:stress, 3:water&stress, 4:water|stress}
Average_CropWaterStress_WindowSpan <- 5;

RunOff_CurveNumber <- 85.0;
RunOff_Ia_SoilType_param <- 0.2;

Plot_Area_1 <- 2500; # surface du plot 1 en m²
Plot_Area_2 <- 2500; # surface du plot 2 en m²
Plot_Area_3 <- 2500; # surface du plot 3 en m²
Plot_Area_4 <- 2500; # surface du plot 4 en m²

#Pump
Pump_A <- 20*14.124;
Pump_B <- -0.783;
Pump_C <- 1.0;
Pump_Altitude <- 600;
Pump_WellDepth <- 100;
Pump_PumpingDuration <- 4;

#Ambhas
Ambhas_hini <- 580;
Ambhas_hmin <- 550;
Ambhas.Sy<- 0.005;
Ambhas.par_discharge <- 0.98;

#IrrigationPriority
a <- 0.4
b <- 0
c <- 0.3
d <- 0.3
RS_strategy <- 1
RC_strategy <- 1
BelePriorities <- list(4,3,2,1)

# Pond
X0 <- 10;
Y0 <- 10;
Z0 <- 5;
fx <- 1;
Init_Pond_Frac <- 0.;
Evap_coeff <- 0.5;
Percol_coeff <- 0.05;


#FarmIrrigationManager
isPondForIrrigation <- 1;
isOptimize <- 1;
isRefill <- 1;


KC_DATADIR <- '../../ASCROM/data/crops/kc/';
KC_DATA <- data.frame(id=c("ex1", "ex2"), 
                      filename=c("kc_sample.txt", "kc_sample2.txt"),
                      stringsAsFactors=FALSE)
KC_T0 <- 0;
KC_SEQ <- list(plot1=data.frame(sequence=c("ex1", "ex2"),
                                padding=0,
                                startdate=c("1/3/2014", "02/4/2014"),
                                enddate=c("01/4/2014", "03/5/2014"),
                                stringsAsFactors=FALSE),
               plot2=data.frame(sequence=c("ex1", "ex2"),
                                padding=0,
                                startdate=c("1/3/2014", "02/4/2014"),
                                enddate=c("01/4/2014", "03/5/2014"),
                                stringsAsFactors=FALSE),
               plot3=data.frame(sequence=c("ex1", "ex2"),
                                padding=0,
                                startdate=c("1/3/2014", "02/4/2014"),
                                enddate=c("01/4/2014", "03/5/2014"),
                                stringsAsFactors=FALSE),
               plot4=data.frame(sequence=c("ex1", "ex2"),
                                padding=0,
                                startdate=c("1/3/2014", "02/4/2014"),
                                enddate=c("01/4/2014", "03/5/2014"),
                                stringsAsFactors=FALSE))

#liste des paramètres attendus dans les fichiers sol: TH1, ThetaFC1, ThetaWP1, BD1, TH2, ThetaFC2, ThetaWP2, BD2, REW1, REW2, Rain_Threshold
SOIL_DATADIR <- "../../ASCROM/data/soils/";
SOIL_ID <- "sol_Berambadi"; #repose sur l'existance d'un fichier {SOIL_ID}.txt dans le dossier {SOIL_DATADIR}

CROP_DATAFILE <- '../../ASCROM/data/crops/crop_parameters.txt';

if (DO_EXPORT) save(list=ls(), file="MF1_2_params.rdata")

########################


library("rvle");
library("chron");
library("readr")
options(chron.origin=c(month=11, day=24, year=-4713));
sessionInfo();

# lecture du vpz
mm <- new("Rvle", pkg="MF1_2", file="MF1_2_ASCROM.vpz");

# passage des vues en mode storage (pour recuperer les vues via la fonction results)
myOutputList <- rep("storage", length(getDefault(mm,"outputplugin")))
names(myOutputList) <- names(getDefault(mm,"outputplugin"))


{## modifications auto de kc_params, h_params à partir de la séquence utilisateur
  
  all_crop_params <- read_csv2(CROP_DATAFILE, col_types=cols(CropID = col_character(), h = col_double()));
  
  KC_LIST <- sapply(unique(do.call(rbind, KC_SEQ)$sequence), 
                    function(kc_ids){read_csv2(paste0(KC_DATADIR, KC_DATA$filename[KC_DATA$id==kc_ids]), col_types=cols(date = col_integer(), kc = col_double()));},
                    simplify=FALSE, USE.NAMES=TRUE)
  
  getSamplePair <- function(RvleObj, condPortName, T0) {
    sample_pair <- getDefault(RvleObj, condPortName);
    sample_pair$t0 <- as.integer(T0)
    for (i in 2:length(sample_pair$pairs)) {
      sample_pair$pairs[[2]] <- NULL
    }
    return(sample_pair)
  }
  
  generateKcPairs <- function(RvleObj, condPortName, T0, crop_seq, kc_list, debut_sim) {
    error_kc <- FALSE
    kc_params <- getSamplePair(RvleObj, condPortName, T0)
    sample_kc_pair <- kc_params$pairs[[1]]

    #generation auto de la sequence complete de paires à partir de la sequence d'id
    kc_pair_index <- 1 # index intégré de la paire kc en cours
    padding <- 0 # decallage intégré (sequence + padding) pour la sequence en cours 
    for (idseq_index in 1:nrow(crop_seq)) {#idseq_index=1
      if (idseq_index==1) {
        crop_seq$padding[idseq_index] <- as.integer(chron(crop_seq$startdate[idseq_index], format=c(dates = "d/m/Y")) - chron(debut_sim, format=c(dates = "Y-m-d")))
      } else {
        crop_seq$padding[idseq_index] <- as.integer(chron(crop_seq$startdate[idseq_index], format=c(dates = "d/m/Y")) - chron(crop_seq$enddate[idseq_index-1], format=c(dates = "d/m/Y")))
      }
      {
        if (crop_seq$padding[idseq_index]<1) {
          if (idseq_index==1) {
            cat("Attention date de debut de la séquence avant le début de simulation\n")
            cat(as.character(chron(debut_sim, format=c(dates = "Y-m-d"), out.format=c(dates = "d/m/Y"))), " < ", as.character(chron(crop_seq$startdate[idseq_index], format=c(dates = "d/m/Y"))), "\n")
          } else {
            cat("Attention date de debut avant date de fin du précédent\n")
            cat(as.character(chron(crop_seq$startdate[idseq_index], format=c(dates = "d/m/Y"))), " < ", as.character(chron(crop_seq$enddate[idseq_index-1], format=c(dates = "d/m/Y"))), "\n",
                idseq_index, " vs ", idseq_index-1, " (element de la séquence)\n")
          }
          error_kc <- TRUE;
        }
        dureeDates <- as.integer(chron(crop_seq$enddate[idseq_index], format=c(dates = "d/m/Y")) - chron(crop_seq$startdate[idseq_index], format=c(dates = "d/m/Y")))
        dureePaires <- max(kc_list[[crop_seq$sequence[idseq_index]]]$date)
        if (dureeDates!=dureePaires) {
          cat("Attention durée différentes entre dates et fichiers de kc\n")
          cat(idseq_index, "eme element de la séquence, ID:", crop_seq$sequence[idseq_index], "\n")
          cat(dureeDates, "(dates séquence) != ", dureePaires, "(durée paires)\n")
          error_kc <- TRUE;
        }
      }
      padding <- padding+crop_seq$padding[idseq_index]
      for (idpair_index in 1:nrow(kc_list[[crop_seq$sequence[idseq_index]]])) {#idpair_index=1
        kc_params$pairs[[kc_pair_index]] <- sample_kc_pair
        kc_params$pairs[[kc_pair_index]]$kcb <- kc_list[[crop_seq$sequence[idseq_index]]][idpair_index,]$kc
        kc_params$pairs[[kc_pair_index]]$date <- as.integer(kc_list[[crop_seq$sequence[idseq_index]]][idpair_index,]$date + padding)
        kc_pair_index <- kc_pair_index+1
      }
      
      padding <- padding+max(kc_list[[crop_seq$sequence[idseq_index]]]$date)
    }
    
    return(list(KC_PARAMS=kc_params,
                KC_SEQ=crop_seq,
                ERROR_KC=error_kc));
    
  }

  generatePairParameters <- function(RvleObj, condPortName, mapKeyName, T0, crop_params_datas, crop_seq, kc_pairs) {#RvleObj=mm; condPortName="condClimaticWaterDemand.h_params"; mapKeyName="h"; T0=KC_T0; crop_params_datas=all_crop_params; crop_seq=KC_SEQ; kc_pairs=KC_LIST;
    
    XX_params <- getSamplePair(RvleObj, condPortName, T0);
    sample_XX_pair <- XX_params$pairs[[1]];

    padding <- 0; # decallage intégré (sequence + padding) pour la sequence en cours 
    
    for (idseq_index in 1:nrow(crop_seq)) {#idseq_index=1
      padding <- padding+crop_seq$padding[idseq_index];
      
      XX_params$pairs[[(idseq_index-1)*4+1]] <- sample_XX_pair;
      XX_params$pairs[[(idseq_index-1)*4+1]][[mapKeyName]] <- 0;
      XX_params$pairs[[(idseq_index-1)*4+1]]$date <- as.integer(kc_pairs[[crop_seq$sequence[idseq_index]]][1,]$date + padding);
      
      XX_params$pairs[[(idseq_index-1)*4+2]] <- sample_XX_pair;
      XX_params$pairs[[(idseq_index-1)*4+2]][[mapKeyName]] <- crop_params_datas[[mapKeyName]][crop_params_datas$CropID==crop_seq$sequence[idseq_index]];
      XX_params$pairs[[(idseq_index-1)*4+2]]$date <- as.integer(kc_pairs[[crop_seq$sequence[idseq_index]]][1,]$date + padding+1);
      
      XX_params$pairs[[(idseq_index-1)*4+3]] <- sample_XX_pair;
      XX_params$pairs[[(idseq_index-1)*4+3]][[mapKeyName]] <- crop_params_datas[[mapKeyName]][crop_params_datas$CropID==crop_seq$sequence[idseq_index]];
      XX_params$pairs[[(idseq_index-1)*4+3]]$date <- as.integer(kc_pairs[[crop_seq$sequence[idseq_index]]][nrow(kc_pairs[[crop_seq$sequence[idseq_index]]]),]$date + padding-1);

      XX_params$pairs[[(idseq_index-1)*4+4]] <- sample_XX_pair;
      XX_params$pairs[[(idseq_index-1)*4+4]][[mapKeyName]] <- 0;
      XX_params$pairs[[(idseq_index-1)*4+4]]$date <- as.integer(kc_pairs[[crop_seq$sequence[idseq_index]]][nrow(kc_pairs[[crop_seq$sequence[idseq_index]]]),]$date + padding);

      padding <- padding+max(kc_pairs[[crop_seq$sequence[idseq_index]]]$date);
    }

    XX_params$pairs[sapply(XX_params$pairs, is.null)] <- NULL

    return(XX_params);
  }
  
  KC_FUN <- KC_PARAMS <- ERROR_KC <- h_params <- MaxDepthRoots_params <- kcini_params <- kcmid_params <- kcmid_params <- IrrigationDose_params <- Max_Irrigation_Duration_params <- Rain_Threshold_params <- SoilWaterContent_Threshold_params <- CropWaterStress_Threshold_params <- YieldMax_params <- Rain_Threshold_To_Reset_Days_Without_Water_params <- Lethal_Threshold_Days_Without_Water_params <- Lethal_Threshold_CropWaterStress_params <- CropPriority_params <- Irrigation_Technic_Factor_params <- Crop_Cycle_Duration_params <- vector(mode = "list", length = 4);
  for (i in 1:4) { # loop over plots
    KC_FUN[[i]] <- generateKcPairs(mm, "condInterpolator_Plot1.kcb_params", KC_T0, KC_SEQ[[i]], KC_LIST, DEBUT_SIMULATION); 
    KC_PARAMS[[i]] <- KC_FUN[[i]]$KC_PARAMS
    KC_SEQ[[i]] <- KC_FUN[[i]]$KC_SEQ
    ERROR_KC[[i]] <- KC_FUN[[i]]$ERROR_KC
    all_crop_params$Crop_Cycle_Duration <- sapply(KC_LIST[all_crop_params$CropID], function(x){as.numeric(x$date[6]-x$date[2])}) #todo: fix key order

    h_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.h_params", "h", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
    MaxDepthRoots_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.MaxDepthRoots_params", "MaxDepthRoots", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
    kcini_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.kcini_params", "kcini", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
    kcmid_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.kcmid_params", "kcmid", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
    
    IrrigationDose_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.IrrigationDose_params", "IrrigationDose", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
    Max_Irrigation_Duration_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.Max_Irrigation_Duration_params", "Max_Irrigation_Duration", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
    Rain_Threshold_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.Rain_Threshold_params", "Rain_Threshold", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
    SoilWaterContent_Threshold_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.SoilWaterContent_Threshold_params", "SoilWaterContent_Threshold", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
    CropWaterStress_Threshold_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.CropWaterStress_Threshold_params", "CropWaterStress_Threshold", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
  
    YieldMax_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.YieldMax_params", "YieldMax", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
    Rain_Threshold_To_Reset_Days_Without_Water_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.Rain_Threshold_To_Reset_Days_Without_Water_params", "Rain_Threshold_To_Reset_Days_Without_Water", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
    Lethal_Threshold_Days_Without_Water_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.Lethal_Threshold_Days_Without_Water_params", "Lethal_Threshold_Days_Without_Water", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
    Lethal_Threshold_CropWaterStress_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.Lethal_Threshold_CropWaterStress_params", "Lethal_Threshold_CropWaterStress", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
  
    CropPriority_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.CropPriority_params", "CropPriority", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
    Irrigation_Technic_Factor_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.Irrigation_Technic_Factor_params", "Irrigation_Technic_Factor", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
    Crop_Cycle_Duration_params[[i]] <- generatePairParameters(mm, "condInterpolator_Plot1.Crop_Cycle_Duration_params", "Crop_Cycle_Duration", KC_T0, all_crop_params, KC_SEQ[[i]], KC_LIST)
  }
}

{##recupération des paramètres sol à partir de l'identifiant
  
  soil_list <- read_csv2(paste0(SOIL_DATADIR, SOIL_ID, ".txt"), col_types=cols(ParamName = col_character(), ParamValue = col_double()));
  
  sapply(soil_list$ParamName, function(x){assign(x, soil_list$ParamValue[soil_list$ParamName==x], envir = .GlobalEnv)})
}

{
  AWC1 <- (ThetaFC1-ThetaWP1)*BD1*TH1;
  REW1 <- REW_AWC_ratio * AWC1;
  AWC2 <- (ThetaFC2-ThetaWP2)*BD2*TH2;
  REW2 <- REW_AWC_ratio * AWC2;

  getPondVol <- function(x0, y0, z0, fx) {
    fy <- y0 * fx / x0;
    s0 <- x0 * y0;
    x1 <- x0 - fx * z0;
    y1 <- y0 - fy * z0;
    z1 <- x0 / fx;
    s1 <- x1 * y1;
    v0 <- z0 * (s0 + s1 + sqrt(s0*s1)) / 3;
    return(v0)
  }
  V0 <- getPondVol(X0, Y0, Z0, fx);

  Farm_Area <- Plot_Area_1 + Plot_Area_2 + Plot_Area_3 + Plot_Area_4;
  
}

ERRORS_KC <- (ERROR_KC[[1]] & ERROR_KC[[2]] & ERROR_KC[[3]] & ERROR_KC[[4]]) 

if (!ERRORS_KC) {
  
  # lancement de la simulation avec forcage des paramètres
  mm <- setDefault(mm ,
            # General parameters
            simulation_engine.begin_date=DEBUT_SIMULATION,
            simulation_engine.duration=DUREE_SIMULATION,
            cond_generic_with_header_complete.meteo_file=FICHIER_CLIMAT,
            # GroundWater
            cond_Ambhas.hini=Ambhas_hini,
            cond_Ambhas.hmin=Ambhas_hmin,
            cond_Ambhas.par_discharge = Ambhas.par_discharge,
            cond_Ambhas.Sy = (Ambhas.Sy),
            # Farm
              # Farm.Irrigation
            condActualIrrigation.Irrig_Efficiency=fw,  # !! same value is used for all plots !!
            condIrrigationDemand.Rule_Strat=Irrig_Rule_Strat,  # !! same value is used for all plots !!
            condPumpPotential.GWpixelArea=Farm_Area,
            condPumpPotential.Altitude=Pump_Altitude,
            condPumpPotential.CoeffA=Pump_A,
            condPumpPotential.CoeffB=Pump_B,
            condPumpPotential.CoeffC=Pump_C,
            condPumpPotential.WellDepth=Pump_WellDepth,
            condConstant.init_value_PumpingDuration=Pump_PumpingDuration,
            condPond.X0=X0,
            condPond.Y0=Y0,
            condPond.Z0=Z0,
            condPond.fx=fx,
            condPond.Init_Pond_Frac=Init_Pond_Frac,
            condPond.Evap_coeff=Evap_coeff,
            condPond.Percol_coeff=Percol_coeff,
            condIrrigationPriority.a=a,
            condIrrigationPriority.b=b,
            condIrrigationPriority.c=c,
            condIrrigationPriority.d=d,
            condIrrigationPriority.RS_strategy=as.integer(RS_strategy),
            condIrrigationPriority.RC_strategy=as.integer(RC_strategy),
            condIrrigationPriority.BelePriorities.as_single=BelePriorities,
            condFarmIrrigationManager.isPondForIrrigation=as.logical(isPondForIrrigation),
            condFarmIrrigationManager.isOptimize=as.logical(isOptimize),
            condFarmIrrigationManager.isRefill=as.logical(isRefill),
            condFarmIrrigationManager.V0=V0,
            condFarmIrrigationManager.PlotArea_1=Plot_Area_1,
            condFarmIrrigationManager.PlotArea_2=Plot_Area_2,
            condFarmIrrigationManager.PlotArea_3=Plot_Area_3,
            condFarmIrrigationManager.PlotArea_4=Plot_Area_4,
              # Farm.UnitsConversions
            condAggregator.AvailableWater=1000/Farm_Area,
            condSplitter_Drain.Drain_1=Plot_Area_1/(1000),
            condSplitter_Drain.Drain_2=Plot_Area_2/(1000),
            condSplitter_Drain.Drain_3=Plot_Area_3/(1000),
            condSplitter_Drain.Drain_4=Plot_Area_4/(1000),
            condSplitter_RunOff.RunOff_1=Plot_Area_1/(1000),
            condSplitter_RunOff.RunOff_2=Plot_Area_2/(1000),
            condSplitter_RunOff.RunOff_3=Plot_Area_3/(1000),
            condSplitter_RunOff.RunOff_4=Plot_Area_4/(1000),
            condNetRecharge.Irrigation=-1,
            condNetRecharge.Drain=1,
              # Farm.Misc
            condMovingAverage.n=as.integer(Average_CropWaterStress_WindowSpan), # !! same value is used for all plots !!
            # Plot 1
              # Plot1.Crop
            condClimaticWaterDemand_Plot1.Failure_Option=as.integer(Failure_Option), # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot1.fw_Rain_Threshold=fw_Rain_Threshold, # !! same value is used for all plots !!
            condInterpolator_Plot1.kcb_params.as_single=KC_PARAMS[[1]],
            condInterpolator_Plot1.h_params.as_single=h_params[[1]],
            condInterpolator_Plot1.MaxDepthRoots_params.as_single=MaxDepthRoots_params[[1]],
            condInterpolator_Plot1.kcini_params.as_single=kcini_params[[1]],
            condInterpolator_Plot1.kcmid_params.as_single=kcmid_params[[1]],
            condInterpolator_Plot1.Rain_Threshold_To_Reset_Days_Without_Water_params.as_single=Rain_Threshold_To_Reset_Days_Without_Water_params[[1]],
            condInterpolator_Plot1.Lethal_Threshold_Days_Without_Water_params.as_single=Lethal_Threshold_Days_Without_Water_params[[1]],
            condInterpolator_Plot1.Lethal_Threshold_CropWaterStress_params.as_single=Lethal_Threshold_CropWaterStress_params[[1]],
            condInterpolator_Plot1.IrrigationDose_params.as_single=IrrigationDose_params[[1]],
            condInterpolator_Plot1.Max_Irrigation_Duration_params.as_single=Max_Irrigation_Duration_params[[1]],
            condInterpolator_Plot1.Rain_Threshold_params.as_single=Rain_Threshold_params[[1]],
            condInterpolator_Plot1.SoilWaterContent_Threshold_params.as_single=SoilWaterContent_Threshold_params[[1]],
            condInterpolator_Plot1.CropWaterStress_Threshold_params.as_single=CropWaterStress_Threshold_params[[1]],
            condInterpolator_Plot1.YieldMax_params.as_single=YieldMax_params[[1]],
            condInterpolator_Plot1.Irrigation_Technic_Factor_params.as_single=Irrigation_Technic_Factor_params[[1]],
            condInterpolator_Plot1.Crop_Cycle_Duration_params.as_single=Crop_Cycle_Duration_params[[1]],
            condInterpolator_Plot1.CropPriority_params.as_single=CropPriority_params[[1]],
              # Plot1.Soil
            condClimaticWaterDemand_Plot1.fw=fw, # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot1.SoilDepth=TH1+TH2, # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot1.REW1=REW1, # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot1.REW2=REW2, # !! same value is used for all plots !!
            condWaterFlows_Plot1.CurveNumber=RunOff_CurveNumber, # !! same value is used for all plots !!
            condWaterFlows_Plot1.Ia_SoilType_param=RunOff_Ia_SoilType_param, # !! same value is used for all plots !!
            condWaterFlows_Plot1.TH1=TH1, # !! same value is used for all plots !!
            condWaterFlows_Plot1.ThetaFC1=ThetaFC1, # !! same value is used for all plots !!
            condWaterFlows_Plot1.ThetaWP1=ThetaWP1, # !! same value is used for all plots !!
            condWaterFlows_Plot1.BD1=BD1, # !! same value is used for all plots !!
            condWaterFlows_Plot1.TH2=TH2, # !! same value is used for all plots !!
            condWaterFlows_Plot1.ThetaFC2=ThetaFC2, # !! same value is used for all plots !!
            condWaterFlows_Plot1.ThetaWP2=ThetaWP2, # !! same value is used for all plots !!
            condWaterFlows_Plot1.BD2=BD2, # !! same value is used for all plots !!
            # Plot 2
              # Plot2.Crop
            condClimaticWaterDemand_Plot2.Failure_Option=as.integer(Failure_Option), # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot2.fw_Rain_Threshold=fw_Rain_Threshold, # !! same value is used for all plots !!
            condInterpolator_Plot2.kcb_params.as_single=KC_PARAMS[[2]],
            condInterpolator_Plot2.h_params.as_single=h_params[[2]],
            condInterpolator_Plot2.MaxDepthRoots_params.as_single=MaxDepthRoots_params[[2]],
            condInterpolator_Plot2.kcini_params.as_single=kcini_params[[2]],
            condInterpolator_Plot2.kcmid_params.as_single=kcmid_params[[2]],
            condInterpolator_Plot2.Rain_Threshold_To_Reset_Days_Without_Water_params.as_single=Rain_Threshold_To_Reset_Days_Without_Water_params[[2]],
            condInterpolator_Plot2.Lethal_Threshold_Days_Without_Water_params.as_single=Lethal_Threshold_Days_Without_Water_params[[2]],
            condInterpolator_Plot2.Lethal_Threshold_CropWaterStress_params.as_single=Lethal_Threshold_CropWaterStress_params[[2]],
            condInterpolator_Plot2.IrrigationDose_params.as_single=IrrigationDose_params[[2]],
            condInterpolator_Plot2.Max_Irrigation_Duration_params.as_single=Max_Irrigation_Duration_params[[2]],
            condInterpolator_Plot2.Rain_Threshold_params.as_single=Rain_Threshold_params[[2]],
            condInterpolator_Plot2.SoilWaterContent_Threshold_params.as_single=SoilWaterContent_Threshold_params[[2]],
            condInterpolator_Plot2.CropWaterStress_Threshold_params.as_single=CropWaterStress_Threshold_params[[2]],
            condInterpolator_Plot2.YieldMax_params.as_single=YieldMax_params[[2]],
            condInterpolator_Plot2.Irrigation_Technic_Factor_params.as_single=Irrigation_Technic_Factor_params[[2]],
            condInterpolator_Plot2.Crop_Cycle_Duration_params.as_single=Crop_Cycle_Duration_params[[2]],
            condInterpolator_Plot2.CropPriority_params.as_single=CropPriority_params[[2]],
              # Plot2.Soil
            condClimaticWaterDemand_Plot2.fw=fw, # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot2.SoilDepth=TH1+TH2, # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot2.REW1=REW1, # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot2.REW2=REW2, # !! same value is used for all plots !!
            condWaterFlows_Plot2.CurveNumber=RunOff_CurveNumber, # !! same value is used for all plots !!
            condWaterFlows_Plot2.Ia_SoilType_param=RunOff_Ia_SoilType_param, # !! same value is used for all plots !!
            condWaterFlows_Plot2.TH1=TH1, # !! same value is used for all plots !!
            condWaterFlows_Plot2.ThetaFC1=ThetaFC1, # !! same value is used for all plots !!
            condWaterFlows_Plot2.ThetaWP1=ThetaWP1, # !! same value is used for all plots !!
            condWaterFlows_Plot2.BD1=BD1, # !! same value is used for all plots !!
            condWaterFlows_Plot2.TH2=TH2, # !! same value is used for all plots !!
            condWaterFlows_Plot2.ThetaFC2=ThetaFC2, # !! same value is used for all plots !!
            condWaterFlows_Plot2.ThetaWP2=ThetaWP2, # !! same value is used for all plots !!
            condWaterFlows_Plot2.BD2=BD2, # !! same value is used for all plots !!
            # Plot 3
              # Plot3.Crop
            condClimaticWaterDemand_Plot3.Failure_Option=as.integer(Failure_Option), # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot3.fw_Rain_Threshold=fw_Rain_Threshold, # !! same value is used for all plots !!
            condInterpolator_Plot3.kcb_params.as_single=KC_PARAMS[[3]],
            condInterpolator_Plot3.h_params.as_single=h_params[[3]],
            condInterpolator_Plot3.MaxDepthRoots_params.as_single=MaxDepthRoots_params[[3]],
            condInterpolator_Plot3.kcini_params.as_single=kcini_params[[3]],
            condInterpolator_Plot3.kcmid_params.as_single=kcmid_params[[3]],
            condInterpolator_Plot3.Rain_Threshold_To_Reset_Days_Without_Water_params.as_single=Rain_Threshold_To_Reset_Days_Without_Water_params[[3]],
            condInterpolator_Plot3.Lethal_Threshold_Days_Without_Water_params.as_single=Lethal_Threshold_Days_Without_Water_params[[3]],
            condInterpolator_Plot3.Lethal_Threshold_CropWaterStress_params.as_single=Lethal_Threshold_CropWaterStress_params[[3]],
            condInterpolator_Plot3.IrrigationDose_params.as_single=IrrigationDose_params[[3]],
            condInterpolator_Plot3.Max_Irrigation_Duration_params.as_single=Max_Irrigation_Duration_params[[3]],
            condInterpolator_Plot3.Rain_Threshold_params.as_single=Rain_Threshold_params[[3]],
            condInterpolator_Plot3.SoilWaterContent_Threshold_params.as_single=SoilWaterContent_Threshold_params[[3]],
            condInterpolator_Plot3.CropWaterStress_Threshold_params.as_single=CropWaterStress_Threshold_params[[3]],
            condInterpolator_Plot3.YieldMax_params.as_single=YieldMax_params[[3]],
            condInterpolator_Plot3.Irrigation_Technic_Factor_params.as_single=Irrigation_Technic_Factor_params[[3]],
            condInterpolator_Plot3.Crop_Cycle_Duration_params.as_single=Crop_Cycle_Duration_params[[3]],
            condInterpolator_Plot3.CropPriority_params.as_single=CropPriority_params[[3]],
              # Plot3.Soil
            condClimaticWaterDemand_Plot3.fw=fw, # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot3.SoilDepth=TH1+TH2, # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot3.REW1=REW1, # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot3.REW2=REW2, # !! same value is used for all plots !!
            condWaterFlows_Plot3.CurveNumber=RunOff_CurveNumber, # !! same value is used for all plots !!
            condWaterFlows_Plot3.Ia_SoilType_param=RunOff_Ia_SoilType_param, # !! same value is used for all plots !!
            condWaterFlows_Plot3.TH1=TH1, # !! same value is used for all plots !!
            condWaterFlows_Plot3.ThetaFC1=ThetaFC1, # !! same value is used for all plots !!
            condWaterFlows_Plot3.ThetaWP1=ThetaWP1, # !! same value is used for all plots !!
            condWaterFlows_Plot3.BD1=BD1, # !! same value is used for all plots !!
            condWaterFlows_Plot3.TH2=TH2, # !! same value is used for all plots !!
            condWaterFlows_Plot3.ThetaFC2=ThetaFC2, # !! same value is used for all plots !!
            condWaterFlows_Plot3.ThetaWP2=ThetaWP2, # !! same value is used for all plots !!
            condWaterFlows_Plot3.BD2=BD2, # !! same value is used for all plots !!
            # Plot 4
              # Plot4.Crop
            condClimaticWaterDemand_Plot4.Failure_Option=as.integer(Failure_Option), # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot4.fw_Rain_Threshold=fw_Rain_Threshold, # !! same value is used for all plots !!
            condInterpolator_Plot4.kcb_params.as_single=KC_PARAMS[[4]],
            condInterpolator_Plot4.h_params.as_single=h_params[[4]],
            condInterpolator_Plot4.MaxDepthRoots_params.as_single=MaxDepthRoots_params[[4]],
            condInterpolator_Plot4.kcini_params.as_single=kcini_params[[4]],
            condInterpolator_Plot4.kcmid_params.as_single=kcmid_params[[4]],
            condInterpolator_Plot4.Rain_Threshold_To_Reset_Days_Without_Water_params.as_single=Rain_Threshold_To_Reset_Days_Without_Water_params[[4]],
            condInterpolator_Plot4.Lethal_Threshold_Days_Without_Water_params.as_single=Lethal_Threshold_Days_Without_Water_params[[4]],
            condInterpolator_Plot4.Lethal_Threshold_CropWaterStress_params.as_single=Lethal_Threshold_CropWaterStress_params[[4]],
            condInterpolator_Plot4.IrrigationDose_params.as_single=IrrigationDose_params[[4]],
            condInterpolator_Plot4.Max_Irrigation_Duration_params.as_single=Max_Irrigation_Duration_params[[4]],
            condInterpolator_Plot4.Rain_Threshold_params.as_single=Rain_Threshold_params[[4]],
            condInterpolator_Plot4.SoilWaterContent_Threshold_params.as_single=SoilWaterContent_Threshold_params[[4]],
            condInterpolator_Plot4.CropWaterStress_Threshold_params.as_single=CropWaterStress_Threshold_params[[4]],
            condInterpolator_Plot4.YieldMax_params.as_single=YieldMax_params[[4]],
            condInterpolator_Plot4.Irrigation_Technic_Factor_params.as_single=Irrigation_Technic_Factor_params[[4]],
            condInterpolator_Plot4.Crop_Cycle_Duration_params.as_single=Crop_Cycle_Duration_params[[4]],
            condInterpolator_Plot4.CropPriority_params.as_single=CropPriority_params[[4]],
              # Plot4.Soil
            condClimaticWaterDemand_Plot4.fw=fw, # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot4.SoilDepth=TH1+TH2, # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot4.REW1=REW1, # !! same value is used for all plots !!
            condClimaticWaterDemand_Plot4.REW2=REW2, # !! same value is used for all plots !!
            condWaterFlows_Plot4.CurveNumber=RunOff_CurveNumber, # !! same value is used for all plots !!
            condWaterFlows_Plot4.Ia_SoilType_param=RunOff_Ia_SoilType_param, # !! same value is used for all plots !!
            condWaterFlows_Plot4.TH1=TH1, # !! same value is used for all plots !!
            condWaterFlows_Plot4.ThetaFC1=ThetaFC1, # !! same value is used for all plots !!
            condWaterFlows_Plot4.ThetaWP1=ThetaWP1, # !! same value is used for all plots !!
            condWaterFlows_Plot4.BD1=BD1, # !! same value is used for all plots !!
            condWaterFlows_Plot4.TH2=TH2, # !! same value is used for all plots !!
            condWaterFlows_Plot4.ThetaFC2=ThetaFC2, # !! same value is used for all plots !!
            condWaterFlows_Plot4.ThetaWP2=ThetaWP2, # !! same value is used for all plots !!
            condWaterFlows_Plot4.BD2=BD2 # !! same value is used for all plots !!
            );
  
  saveVpz(mm, "tmp.vpz")
  
  mm <- run(mm, outputplugin=myOutputList)
  
  # valeurs des résultats (vue view)
  res <- results(mm)$view
  
  # Conversion of time into calendar date format
  res$dates <- dates(res$`top:MeteoReader.current_date_str`, format = c(dates = "y-m-d"));

  if (DO_EXPORT) write_csv2(res, "SimOutput_MF11.csv")


  if(FALSE){
  # exemple de graph sur les sorties
  filter_regex <- "*"
  for (VarName in colnames(res)[grep(filter_regex, colnames(res))]) {
    if (!VarName%in%c("time", "dates", "top:MeteoReader.current_date_str")) {
      plot(res$dates, res[, VarName==colnames(res)], main=VarName, ylab=tail(strsplit(x=VarName, split=".", fixed=TRUE)[[1]], n=1), type="l")
    }
  }


    print_pair_params <- function(XX_params) {#XX_params=YieldMax_params
      cat("t0=", XX_params$t0, "\n")
      cat("Number of pair values: ", length(XX_params$pairs), "\n")
      for (i in 1:length(XX_params$pairs)) {
        cat(i, " -\t");
        for (j in names(XX_params$pairs[[i]])) {
          cat(j, ": ", XX_params$pairs[[i]][[j]], "\t")
        }
        cat("\n")
      }
    }
    print_pair_params(KC_PARAMS)
    print_pair_params(h_params)
    print_pair_params(YieldMax_params)
    print_pair_params(CropPriority_params)
    print_pair_params(Crop_Cycle_Duration_params)
    print_pair_params(Irrigation_Technic_Factor_params)
    
  
  library("dygraphs"); #dygraph(...) : interactive plot of time series
  library("xts"); #xts(...) : time series class for dygraph
  
  # Conversion of time into calendar date format
  res$time <- dates(res$`top:MeteoReader.current_date_str`, format = c(dates = "y-m-d"));
  
  xts_time <- as.POSIXct(as.character(res$time), format="%m/%d/%y")
  mycol <- "darkblue";
  
  
  p <- list()
  for (VarName in colnames(res)) {
    if (!VarName%in%c("time", "dates", "top:MeteoReader.current_date_str")) {
      Var <- res[, VarName==colnames(res)]
      p[[VarName]] <- dygraph(xts(Var, xts_time), ylab=VarName, main=VarName)%>%
        dyOptions(fillGraph = TRUE, fillAlpha = 0.4) %>%
        dySeries("V1", label=VarName, drawPoints = TRUE, color = mycol);
      
    }
  }
  p

  }




  if(FALSE){
    # Pond Water Balance
    S0 <- X0 * Y0;
    n <- length(Pond.Volume) - 1;

    Pond.Volume <- res$"top,FarmModel:Pond.V";
    Pond.Volume.Delta <- c(0, diff(Pond.Volume))

    Pond.Input.RunOff <- c(0,res$"top,FarmModel:Splitter_RunOff.WeightedSum"[-n]); # nosync
    Pond.Input.Rain <- res$"top:MeteoReader.Rain" * S0 / 1000.
    Pond.Input.PumpRefill <- res$"top,FarmModel:Pond.Pump_water_refill";
    Pond.Inputs <- Pond.Input.RunOff + Pond.Input.Rain + Pond.Input.PumpRefill;

    Pond.Output.Evaporation <- res$"top,FarmModel:Pond.Evaporation";
    Pond.Output.Percolation <- res$"top,FarmModel:Pond.Percolation";
    Pond.Output.Overflow <- res$"top,FarmModel:Pond.Pond_Overflow";
    Pond.Output.Irrigation <- res$"top,FarmModel,IrrigationManager:FarmIrrigationManager.PondExtraction";
    Pond.Outputs <- Pond.Output.Evaporation + Pond.Output.Percolation + Pond.Output.Overflow + Pond.Output.Irrigation;

    Pond.Balance <- Pond.Volume.Delta - Pond.Inputs + Pond.Outputs;

    plot(Pond.Volume, type="l")
    plot(diff(Pond.Volume), type="l")
    plot(Pond.Inputs, type="l")
    plot(Pond.Outputs, type="l")
    plot(Pond.Balance, type="l")

    View(data.frame(Balance=Pond.Balance,
                    Delta=Pond.Volume.Delta,
                    Inputs=(-Pond.Inputs),
                    Outputs=Pond.Outputs,
                    Input.RunOff=Pond.Input.RunOff,
                    Input.Rain=Pond.Input.Rain,
                    Input.PumpRefill=Pond.Input.PumpRefill,
                    Output.Evaporation=Pond.Output.Evaporation,
                    Output.Percolation=Pond.Output.Percolation,
                    Output.Overflow=Pond.Output.Overflow,
                    Output.Irrigation=Pond.Output.Irrigation
                    ))

    library("dygraphs"); #dygraph(...) : interactive plot of time series
    library("xts"); #xts(...) : time series class for dygraph

    res$time <- dates(res$`top:MeteoReader.current_date_str`, format = c(dates = "y-m-d"));
    xts_time <- as.POSIXct(as.character(res$time), format="%m/%d/%y")

    mycol <- "blue";
    dygroupname <- "single";

    Var <- Pond.Volume;

    dygraph(xts(Var, xts_time), ylab="Pond volume m3", main=paste0("Pond volume m3"), group=dygroupname)%>%
      dyOptions(stackedGraph=TRUE, includeZero=TRUE) %>%
      dyAxis("y", label = "m3") %>%
      dySeries("V1", label = "Pond.Volume", color="blue") %>%
      dyLegend(labelsSeparateLines = TRUE);

    Var <- cbind(Pond.Input.RunOff,Pond.Input.Rain,Pond.Input.PumpRefill);

    dygraph(xts(Var, xts_time), ylab="Pond Inputs volume m3", main=paste0("Pond Inputs volume m3"), group=dygroupname)%>%
      dyOptions(stackedGraph=TRUE, includeZero=TRUE) %>%
      dyAxis("y", label = "m3") %>%
      dySeries("Pond.Input.RunOff", label = "Pond.Input.RunOff", color="blue") %>%
      dySeries("Pond.Input.Rain", label = "Pond.Input.Rain", color="green") %>%
      dySeries("Pond.Input.PumpRefill", label = "Pond.Input.PumpRefill", color="orange") %>%
      dyLegend(labelsSeparateLines = TRUE);



    Var <- cbind(Pond.Output.Evaporation, Pond.Output.Percolation, Pond.Output.Overflow, Pond.Output.Irrigation);

    dygraph(xts(Var, xts_time), ylab="Pond Outputs volume m3", main=paste0("Pond Outputs volume m3"), group=dygroupname)%>%
      dyOptions(stackedGraph=TRUE, includeZero=TRUE) %>%
      dyAxis("y", label = "m3") %>%
      dySeries("Pond.Output.Evaporation", label = "Pond.Output.Evaporation", color="blue") %>%
      dySeries("Pond.Output.Percolation", label = "Pond.Output.Percolation", color="green") %>%
      dySeries("Pond.Output.Overflow", label = "Pond.Output.Overflow", color="orange") %>%
      dySeries("Pond.Output.Irrigation", label = "Pond.Output.Irrigation", color="red") %>%
      dyLegend(labelsSeparateLines = TRUE);



    Var <- cbind(Pond.Input.RunOff,Pond.Input.Rain,Pond.Input.PumpRefill,
                 -cbind(Pond.Output.Evaporation, Pond.Output.Percolation, Pond.Output.Overflow, Pond.Output.Irrigation));

    dygraph(xts(Var, xts_time), ylab="Pond Flows volume m3", main=paste0("Pond Flows volume m3"), group=dygroupname)%>%
      dyOptions(stackedGraph=TRUE, includeZero=TRUE) %>%
      dyAxis("y", label = "m3") %>%
      dySeries("Pond.Output.Evaporation", label = "Pond.Output.Evaporation", color="blue") %>%
      dySeries("Pond.Output.Percolation", label = "Pond.Output.Percolation", color="green") %>%
      dySeries("Pond.Output.Overflow", label = "Pond.Output.Overflow", color="orange") %>%
      dySeries("Pond.Output.Irrigation", label = "Pond.Output.Irrigation", color="red") %>%
      dySeries("Pond.Output.Evaporation", label = "Pond.Output.Evaporation", color="blue") %>%
      dySeries("Pond.Output.Percolation", label = "Pond.Output.Percolation", color="green") %>%
      dySeries("Pond.Output.Overflow", label = "Pond.Output.Overflow", color="orange") %>%
      dySeries("Pond.Output.Irrigation", label = "Pond.Output.Irrigation", color="red") %>%
      dyLegend(labelsSeparateLines = TRUE);





    # GroundWater Water Balance


    GroundWater.Volume <- res$"top:Ambhas.GWVol";
    GroundWater.Volume.Delta <- c(0, diff(GroundWater.Volume))

    GroundWater.Input.Drain_Plot1 <- res$"top,FarmModel,Plot_1:WaterFlows.Drain" * Plot_Area_1 / 1000.;
    GroundWater.Input.Drain_Plot2 <- res$"top,FarmModel,Plot_2:WaterFlows.Drain" * Plot_Area_2 / 1000.;
    GroundWater.Input.Drain_Plot3 <- res$"top,FarmModel,Plot_3:WaterFlows.Drain" * Plot_Area_3 / 1000.;
    GroundWater.Input.Drain_Plot4 <- res$"top,FarmModel,Plot_4:WaterFlows.Drain" * Plot_Area_4 / 1000.;
    GroundWater.Input.Pond_Percolation <- res$"top,FarmModel:Pond.Percolation";
    GroundWater.Inputs <- GroundWater.Input.Drain_Plot1 + GroundWater.Input.Drain_Plot2 + GroundWater.Input.Drain_Plot3 + GroundWater.Input.Drain_Plot4 + GroundWater.Input.Pond_Percolation;

    GroundWater.Output.Pump_Irrigation <- res$"top,FarmModel,IrrigationManager:FarmIrrigationManager.PumpExtraction";
    GroundWater.Output.GW_Discharge <- res$"top:Ambhas.discharge"*Farm_Area;
    GroundWater.Outputs <- GroundWater.Output.Pump_Irrigation + GroundWater.Output.GW_Discharge;

    GroundWater.Balance <- GroundWater.Volume.Delta - GroundWater.Inputs + GroundWater.Outputs;



    View(data.frame(Volume=GroundWater.Volume,
                    Balance=GroundWater.Balance,
                    Delta=GroundWater.Volume.Delta,
                    Inputs=(-GroundWater.Inputs),
                    Outputs=GroundWater.Outputs,
                    Input.Drain_Plot1=GroundWater.Input.Drain_Plot1,
                    Input.Drain_Plot2=GroundWater.Input.Drain_Plot2,
                    Input.Drain_Plot3=GroundWater.Input.Drain_Plot3,
                    Input.Drain_Plot4=GroundWater.Input.Drain_Plot4,
                    Input.Pond_Percolation=GroundWater.Input.Pond_Percolation,
                    Output.Pump_Irrigation=GroundWater.Output.Pump_Irrigation,
                    Output.GW_Discharge=GroundWater.Output.GW_Discharge
    ))



    xts_time <- as.POSIXct(as.character(res$dates), format="%m/%d/%y")


    mycol <- "darkblue";
    dygroupname <- "gw_balance";

    Var <- GroundWater.Volume;

    p <- dygraph(xts(Var, xts_time), ylab="GroundWater Volume m3", main=paste0("GroundWater Volume m3"), group=dygroupname)%>%
      dyOptions(stackedGraph=TRUE, includeZero=TRUE) %>%
      dyAxis("y", label = "m3") %>%
      dySeries("V1", label = "GW.Volume", color=mycol) %>%
      dyLegend(labelsSeparateLines = TRUE);

    p


    Var <- GroundWater.Balance;

    p <- dygraph(xts(Var, xts_time), ylab="GroundWater daily Mass Balance m3", main=paste0("GroundWater daily Mass Balance m3"), group=dygroupname)%>%
      dyOptions(stackedGraph=TRUE, includeZero=TRUE) %>%
      dyAxis("y", label = "m3") %>%
      dySeries("V1", label = "GW.Balance", color=mycol) %>%
      dyLegend(labelsSeparateLines = TRUE);

    p




    Var <- cbind(GroundWater.Input.Drain_Plot1, GroundWater.Input.Drain_Plot2, GroundWater.Input.Drain_Plot3, GroundWater.Input.Drain_Plot4, GroundWater.Input.Pond_Percolation,
                 -cbind(GroundWater.Output.Pump_Irrigation, GroundWater.Output.GW_Discharge));
    colnames(Var) <- paste0("V", 1:7)

    p <- dygraph(xts(Var, xts_time), ylab="GroundWater daily Net Flows volume m3", main=paste0("GroundWater daily Net Flows volume m3"), group=dygroupname)%>%
      dyOptions(stackedGraph=TRUE, includeZero=TRUE) %>%
      dyAxis("y", label = "m3") %>%
      dySeries("V6", label = "GW.Output.Pump_Irrigation", color="darkgreen") %>%
      dySeries("V7", label = "GW.Output.GW_Discharge", color="darkorange") %>%
      dySeries("V1", label = "GW.Input.Drain_Plot1", color="blue") %>%
      dySeries("V2", label = "GW.Input.Drain_Plot2", color="green") %>%
      dySeries("V3", label = "GW.Input.Drain_Plot3", color="orange") %>%
      dySeries("V4", label = "GW.Input.Drain_Plot4", color="red") %>%
      dySeries("V5", label = "GW.Input.Pond_Percolation", color="darkblue") %>%
      dyLegend(labelsSeparateLines = TRUE);

    p


    Var <- cbind(GroundWater.Input.Drain_Plot1,GroundWater.Input.Drain_Plot2,GroundWater.Input.Drain_Plot3, GroundWater.Input.Drain_Plot4, GroundWater.Input.Pond_Percolation);
    colnames(Var) <- paste0("V", 1:5)

    p <- dygraph(xts(Var, xts_time), ylab="GroundWater daily Inputs flows volume m3", main=paste0("GroundWater daily Inputs flows volume m3"), group=dygroupname)%>%
      dyOptions(stackedGraph=TRUE, includeZero=TRUE) %>%
      dyAxis("y", label = "m3") %>%
      dySeries("V1", label = "GW.Input.Drain_Plot1", color="blue") %>%
      dySeries("V2", label = "GW.Input.Drain_Plot2", color="green") %>%
      dySeries("V3", label = "GW.Input.Drain_Plot3", color="orange") %>%
      dySeries("V4", label = "GW.Input.Drain_Plot4", color="red") %>%
      dySeries("V5", label = "GW.Input.Pond_Percolation", color="darkblue") %>%
      dyLegend(labelsSeparateLines = TRUE);

    p


    Var <- cbind(GroundWater.Output.Pump_Irrigation, GroundWater.Output.GW_Discharge);
    colnames(Var) <- paste0("V", 1:2)

    p <- dygraph(xts(Var, xts_time), ylab="GroundWater daily Outputs flows volume m3", main=paste0("GroundWater daily Outputs flows volume m3"), group=dygroupname)%>%
      dyOptions(stackedGraph=TRUE, includeZero=TRUE) %>%
      dyAxis("y", label = "m3") %>%
      dySeries("V1", label = "GW.Output.Pump_Irrigation", color="blue") %>%
      dySeries("V2", label = "GW.Output.GW_Discharge", color="green") %>%
      dyLegend(labelsSeparateLines = TRUE);

    p

  }


  YearlySum <- function(values, dates) {
    sapply(unique(as.character(years(dates))), function(x, vals=values, dt=dates) {sum(vals[as.character(years(dt))==x])})
  }

  # YearlySum(rep(1,1000), res$dates[1:1000])
  # YearlySum(rep(1,1000), res$dates[1:1000+10])
  # YearlySum(res$`top:MeteoReader.Rain`, res$dates)



  kcseq_dates <- KC_SEQ
  kcseq_dates$startdate <- chron(kcseq_dates$startdate, format=c(dates = "d/m/Y"))
  kcseq_dates$enddate <- chron(kcseq_dates$enddate, format=c(dates = "d/m/Y"))

  CropSeqSum <- function(values, dates, kcseq_dates) {
    sapply(kcseq_dates$sequence, function(x, vals=values, dt=dates){sum(vals[kcseq_dates$startdate[kcseq_dates$sequence==x] < dates & dates< kcseq_dates$enddate[kcseq_dates$sequence==x]], na.rm=TRUE)})
  }

#  CropSeqSum(rep(1,1000), res$dates[1:1000], kcseq_dates[1:3,])
#  CropSeqSum(res$`top:MeteoReader.Rain`, res$dates, kcseq_dates)
#  CropSeqSum(res$`top:MeteoReader.Rain`, res$dates, kcseq_dates[1:6,])



}
############################
#resultats dans fichier
############################
#sink("/home/record/Documents/R/Result/nom_fichier.csv")
#print(resultat_voulu)

############################
#resultats sur ecran
############################
#sink()
#print(resultat_voulu)
