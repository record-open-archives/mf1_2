
# MF1_2

> An integrated Farm model model.

This model has been developped during the [Atcha project](https://www6.inrae.fr/atcha/).  

The documentation of the model is available at https://record-open-archives.pages.mia.inra.fr/mf1_2/

An corresponding interactive shiny application is available at https://mf-12-1-ha.sk8.inrae.fr/

## Requirements

* [VLE-2.0.2](https://github.com/vle-forge/vle/releases/tag/v2.0.2) ([requirements](https://github.com/vle-forge/vle/tree/master2.0#requirements))
* VLE packages
  * [vle.discrete-time](https://github.com/vle-forge/packages/tree/82ee9dfbae1fb24cdbdb39ea9ff7eee19eb062fe/vle.discrete-time)
  * [vle.reader](https://github.com/vle-forge/packages/tree/82ee9dfbae1fb24cdbdb39ea9ff7eee19eb062fe/vle.reader)
  * [vle.tester](https://github.com/vle-forge/packages/tree/82ee9dfbae1fb24cdbdb39ea9ff7eee19eb062fe/vle.tester)
  * [record.meteo](https://forgemia.inra.fr/record/RECORD/-/tree/59608cf39ce147048aaafc4c516d9bc04fcd22aa/pkgs/record.meteo)
  * [vle.discrete-time.generic](https://forgemia.inra.fr/record/RECORD/-/tree/59608cf39ce147048aaafc4c516d9bc04fcd22aa/pkgs/vle.discrete-time.generic)
  * [Ambhas_Cpp](https://forgemia.inra.fr/record-open-archives/ambhas_cpp/-/tree/688247efa3e11b0397ecbca5a46391b4d98fd8c0)
  * [PumpPotential](https://forgemia.inra.fr/record-open-archives/pump-potential/-/tree/6861b8c0048ccf019518dd21546bf6fd8aa95d52)
  * [ASCROM](https://forgemia.inra.fr/record-open-archives/ascrom/-/tree/9d28907e8ebfe96867375709d9ac798a654fcb21)

## Build/installation instructions

### Local build

Once the above requirements are met install the MF1_2 VLE package by running the following command from the root directory of this project

```shell
vle -P MF1_2 clean rclean configure build test
```
### Using docker

Alternativly a [docker](https://www.docker.com/get-started/) image satisfying some of the above requirements is available in a public docker registry at registry.forgemia.inra.fr/record/record@sha256:93cb949a4228b4da336490900b523483503a07ab07eb8a3b438b66c7e75919e5.  
Once docker is installed, run an interactive container with the following command from the root directory of this project

```shell
docker run \
       --rm \
       -it \
       --user root \
       -v "$(pwd):/work" \
       registry.forgemia.inra.fr/record/record@sha256:93cb949a4228b4da336490900b523483503a07ab07eb8a3b438b66c7e75919e5 \
       bash
```

Then from inside the container run the commands

```shell
apt-get update
apt-get install --no-install-recommends --no-install-suggests -y git

cd /tmp
git clone https://forgemia.inra.fr/record-open-archives/ambhas_cpp.git
cd ambhas_cpp
git reset --hard 688247efa3e11b0397ecbca5a46391b4d98fd8c0
vle -P Ambhas_Cpp clean rclean configure build

cd /tmp
git clone https://forgemia.inra.fr/record-open-archives/pump-potential.git
cd pump-potential
git reset --hard 6861b8c0048ccf019518dd21546bf6fd8aa95d52
vle -P PumpPotential clean rclean configure build

cd /tmp
git clone https://forgemia.inra.fr/record-open-archives/ascrom.git
cd ascrom
git reset --hard 9d28907e8ebfe96867375709d9ac798a654fcb21
vle -P ASCROM clean rclean configure build

cd /work
vle -P MF1_2 clean rclean configure build test
```

## Licence

GPLv3 or later. See the file MF1_2/Licence.txt. Some files are under a different license. Check the headers for the copyright info.
